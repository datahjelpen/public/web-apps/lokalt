FROM php:7.4-fpm
WORKDIR /usr/share/nginx/html

RUN docker-php-ext-install pdo pdo_mysql

RUN apt update
RUN apt install -y nginx

COPY nginx-site.conf /etc/nginx/sites-enabled/default
COPY entrypoint.sh /etc/entrypoint.sh
COPY --chown=www-data:www-data src .
RUN chmod +x /etc/entrypoint.sh

EXPOSE 80 443
ENTRYPOINT ["sh", "/etc/entrypoint.sh"]